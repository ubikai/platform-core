from app.routes import *
import string
import random
import json


@app.route("/admin", methods=['GET', 'POST'])
@login_required
@admin_required
def admin():
    if request.method == 'GET':
        return render_template("admin.html", project_color=project_color, user=current_user)


@app.route("/admin/keys", methods=['POST'])
@login_required
@admin_required
def admin_keys():
    if request.method == 'POST':
        keys = []
        data = request.form.to_dict()
        users = int(data["users_number"])

        dprint('Data = ', data)

        letters_and_digits = string.ascii_letters + string.digits
        for i in range(users):
            key_gen = ''.join(random.choice(letters_and_digits)
                              for _ in range(10))
            key = AccessKey(
                key=key_gen,
                counter=1
            )
            key.submit()
            keys.append(key_gen)
            dprint("Key - {}".format(key_gen))
        return str(keys)
