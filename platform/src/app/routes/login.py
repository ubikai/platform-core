from app.routes import *
from app.forms import LoginForm, RegisterForm


@app.route("/register", methods=['GET', 'POST'])
def register():
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    form = RegisterForm(csrf_enabled=False)

    if request.method == 'POST':
        user_id = request.form.get('user_id')
        register_key = request.form.get('register_key')
        ac = db.session.query(AccessKey).filter(AccessKey.key == register_key).first()

        if form.validate_on_submit and ac:
            user_data = get_user_data(user_id)
            if not user_data.get('user_id'):
                err = 'There was a problem. (REGISTER_APP_USER_NONE)'
                dprint(err)
                flash(err)
                return render_template("default/register.html", title='Register', form=form)
            # noinspection PyArgumentList
            u = User(
                email=form.email.data.lower(),
                bot_id=user_data['user_id']
            )
            u.set_password(form.password.data)
            ud = u.data.append(UserData(data=Data.get_data("first_name"), value=user_data['first_name']))
            ud = u.data.append(UserData(data=Data.get_data("last_name"), value=user_data['last_name']))
            if user_data.get('profile_pic'):
                ud = u.data.append(UserData(data=Data.get_data("profile_pic"), value=user_data['profile_pic']))

            u.submit()

            ac.counter -= 1
            if ac.counter < 1:
                ac.delete()
            else:
                ac.submit()

            login_user(u, remember=True)

            return redirect(url_for('login'))
        else:
            for field_name, error_message in form.errors.items():
                flash(error_message[0])
                break

        return render_template("default/register.html", title='Register', form=form, register_key=register_key)
    return render_template("default/register.html", title='Register', form=form)


@app.route('/login', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    form = LoginForm(csrf_enabled=False)
    if request.method == 'POST':
        user = User.query.filter_by(email=form.email.data).first()
        if user is None or not user.check_password(form.password.data):
            flash('Invalid username or password')
            return redirect('/')
        login_user(user, remember=form.remember_me.data)
        return redirect('/')
    return render_template('default/login.html', title='Sign In', form=form)


@app.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect('/')
