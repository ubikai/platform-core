function DrawApexRangeHeatmap(data, div_id) {
    var options = {
      chart: {
        height: '384',
        width: '100%',
        type: 'heatmap',
        animations: {
            enabled: true
        },
        toolbar: {
            show: false
        },
      },
      tooltip: {
        enabled: false
      },
      plotOptions: {
        heatmap: {
          enableShades: false,
          colorScale: {
            ranges: data['ranges']
          }
        }
      },
      dataLabels: {
        enabled: false
      },
      series: data['data'],
      xaxis: {
        categories: data['xaxis']['categories']
      },
    }
    var chart = new ApexCharts(
      document.querySelector('#'+div_id),
      options
    );

    chart.render();
}

function DrawApexDonut(data, div_id) {
    var options = {
      chart: {
        height: '320',
        width: '100%',
        type: 'donut',
         animations: {
            enabled: true
         },
         toolbar: {
            show: false
         },
      },
      tooltip: {
        enabled: false
      },
      series: data['data'],
      labels: data['labels'] || [],
      legend: data['legend'] || {"position": "top"},
      colors: data['colors'] || ['#000'],
      dataLabels: {
        enabled: true,
        formatter: function (val) {
          return Math.floor(val) + "%"
        },
      }
    }
    var chart = new ApexCharts(
      document.querySelector('#'+div_id),
      options
    );

    chart.render();
}

function DrawApexRadarPolygon(data, div_id){
            var options = {
            chart: {
                height: 350,
                type: 'radar',
                toolbar: {
                    show: false
                },
            },
            series: data['data'],
            labels: data['labels'] || [],
            plotOptions: {
                radar: {
                    size: 135,
                    polygons: {
                        strokeColor: '#e9e9e9',
                        fill: {
                            colors: ['#f8f8f8', '#fff']
                        }
                    }
                }
            },
            xaxis: {
              labels: {
               show: false,
                formatter: function (value) {
                    if (data['mobile_labels']){
                        var mobile_labels = data['mobile_labels']
                        return mobile_labels[value - 1];
                    }
                    return value;
                }
              }
            },
            colors: ["#00e396", '#ff4560'],
            tooltip: {
                y: {
                    formatter: function(val) {
                        return val + (data['val_indicator'] || '')
                    }
                }
            },
        }
        var chart = new ApexCharts(
            document.querySelector("#"+div_id),
            options
        );
        chart.render();
}