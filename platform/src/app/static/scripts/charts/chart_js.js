Chart.defaults.global.defaultFontSize = 24;
Chart.defaults.doughnut.animation.animateScale = true; // notg00d4slowphones
Chart.defaults.doughnut.animation.animateRotate = true; // notg00d4slowphones

function DrawDoughnut(values, labels, legend, bgColors, id){
    new Chart(document.getElementById(id), {
        type: 'doughnut',
        data: {
          labels: labels,
          datasets: [
            {
              backgroundColor: bgColors,
              data: values
            }
          ]
        },
        options: {
            maintainAspectRatio: false,
            responsive: true,
            legend: {
                position: 'bottom',
            },
            title: {
                display: true,
                text: legend
            },
            tooltips: {
                callbacks: {
                    label: function(tooltipItem, data) {
                        var dataset = data.datasets[tooltipItem.datasetIndex];
                        var total = dataset.data.reduce(function(previousValue, currentValue, currentIndex, array) {
                        return previousValue + currentValue;
                        });
                        var currentValue = dataset.data[tooltipItem.index];
                        var percentage = Math.floor(((currentValue/total) * 100)+0.5);         
                        return percentage + "%";
                    }
                }
            }
        }
    });
}

function DrawBar(values, labels, legend, bgColors, id){
    new Chart(document.getElementById(id), {
        type: 'bar',
        data: {
            labels: labels,
            datasets: [{
                  label: legend,
                  backgroundColor: bgColors,
                  data: values
            }]
        },
        options: {
            maintainAspectRatio: false,
            responsive: true,
            scales: {

                xAxes: [{
                    ticks: {
                        fontSize: 12,
                        fontFamily: "Helvetica"
                    },
                    gridLines: {
                        display:false
                    }
                }],
                yAxes: [{
                    ticks: {
                        beginAtZero: true,
                        fontSize: 12
                    },
                    gridLines: {
                        display:false,
                    }
                }]
            },
            legend: {
                display: false
            },
            title: {
                display: true,
                text: legend
            },
        }
    });
}

function DrawStackedBar(data, id){
    // console.log(data);
    new Chart(document.getElementById(id), {
        type: 'bar',
        data: {
            labels: data["labels"],
            datasets: data["data"],
//            datasets: [{
//                  label: legend,
//                  backgroundColor: bgColors,
//                  data: values
//            },{
//                  label: legend,
//                  backgroundColor: "#000",
//                  data: values
//            }]
        },
        options: {
            maintainAspectRatio: false,
            responsive: true,
            scales: {
                xAxes: [{
                    stacked: true,
                    ticks: {
                        fontSize: 12,
                        fontFamily: "Helvetica"
                    },
                    gridLines: {
                        display:false
                    }
                }],
                yAxes: [{
                    stacked: true,
                    ticks: {
                        beginAtZero: true,
                        fontSize: 12
                    },
                    gridLines: {
                        display:false,
                    }
                }]
            },
            legend: {
                display: false
            },
            title: {
                display: true,
                text: data["legend"]
            },
        }
    });
}
