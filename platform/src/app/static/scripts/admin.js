var key = {
    create: function(){
        var users_number = $("#form_users_number")[0].value;

        alert.clear()
        if (users_number){
            $.post("/admin/keys", {"users_number": users_number}).done(function( data ) {
                console.log(data);
                alert.show(`${users_number} keys generated. (Check F12)`, "success");
            });
        }else{
            alert.show("Please add a number.", "danger");
        }
    }
}
