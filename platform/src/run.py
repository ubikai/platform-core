from app import app, db, models
import sys
sys.path.append("/platform/src")

from app import app, db, models


@app.shell_context_processor
def make_shell_context():
    return {
        'db': db,
        'md': models,
    }


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=80, debug=True)
