from functools import wraps
from flask import g, request, redirect, url_for
from datetime import datetime, timedelta
from flask_login import current_user
import requests
from subprocess import check_output
import sys
from os import environ


def dprint(*args):  # debug print
    print(*args, file=sys.stderr)


def login_required(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if g.user is None:
            return redirect(url_for('login'))
        return f(*args, **kwargs)

    return decorated_function


def get_week_days(year, week):
    d = datetime(year, 1, 1)
    if d.weekday() > 3:
        d = d + timedelta(7 - d.weekday())
    else:
        d = d - timedelta(d.weekday())
    dlt = timedelta(days=(week - 1) * 7)
    return d + dlt, d + dlt + timedelta(days=6)


def admin_required(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if current_user.access_level < 1:
            return redirect(url_for('index'))
        return f(*args, **kwargs)

    return decorated_function


def get_container_ip(cont_name):
    out = check_output(["dig", "+short", cont_name])
    out = out.decode("utf-8")
    out = out.split('\n', 1)[0]
    return out if out is not "" else None


def get_user_data(bot_id):
    data = {
        "bot_id": bot_id
    }

    # get container ip BECAUSE Apache!
    PROJECT_NAME = environ.get("PROJECT_NAME", default=None)
    cont_ip = get_container_ip('{}_bot_1'.format(PROJECT_NAME))

    out = requests.get("http://{}/user".format(cont_ip), params=data)

    if not out:
        return None

    out = out.json()
    print(out, file=sys.stderr)
    return out
