from sys import path

path.append('/platform/src/')

from app.routes import *
import random
import string


def random_string(stringLength):
    return ''.join(random.choice(string.ascii_letters) for i in range(stringLength))


# noinspection PyArgumentList
def create_keywords():
    d = Data(name="first_name")
    d.submit()
    d = Data(name="last_name")
    d.submit()
    d = Data(name="profile_pic")
    d.submit()


# noinspection PyArgumentList
def create_test_user():
    create_keywords()

    u = User(bot_id=1, email="asd", access_level=1)
    u.set_password('asd')

    u.data.append(UserData(data=Data.get_data("first_name"), value="DELETE"))
    u.data.append(UserData(data=Data.get_data("last_name"), value="ME"))
    u.data.append(UserData(data=Data.get_data("profile_pic"),
                                value="http://theasianherald.com/wp-content/uploads/2017/02/16583841_243568769423759_7297710697935273984_n.jpg"))

    u.submit()
    print('User created')


def create_test_users():
    bot_ids = [i for i in range(1, 241)]
    for bot_id in bot_ids:
        user_data = get_user_data(bot_id=bot_id)
        email = 'user_' + str(bot_id)
        password = 'qwe'

        u = User(bot_id=bot_id, email=email, access_level=1)
        u.set_password(password)

        u.data.append(UserData(data=Data.get_data("first_name"), value=user_data['first_name']))
        u.data.append(UserData(data=Data.get_data("last_name"), value=user_data['last_name']))
        u.data.append(UserData(
            data=Data.get_data("profile_pic"),
            value="")
        )

        u.submit()


if __name__ == '__main__':
    create_test_user()  # create 1 user
    # create_test_users()  # create x users (hardcoded)
    pass
