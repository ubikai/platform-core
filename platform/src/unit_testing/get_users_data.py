from sys import path

path.append('/platform/src/')

from app.routes import *
start = datetime.now()
users = User.query.all()

for user in users:
    user_data = get_user_data(bot_id=user.bot_id)
    user.data.append(UserData(data=Data.get_data("first_name"), value=user_data['first_name']))
    user.data.append(UserData(data=Data.get_data("last_name"), value=user_data['last_name']))
    user.data.append(UserData(data=Data.get_data("profile_pic"), value=user_data['profile_pic']))
    print('\n', user.data)
    user.submit()

print("Done in {}".format(datetime.now() - start))
